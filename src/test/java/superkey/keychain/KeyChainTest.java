/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bruno
 */
public class KeyChainTest {
    
    private File usersFile;
    private KeyChain kc;
    private KeyEntry entryOne, entryTwo;
    
    public KeyChainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws IOException {
        
        usersFile=new File("Keychain.txt");
        kc=KeyChain.from(usersFile, new CipherTool("#wisper"));
        entryOne = new KeyEntry();
        entryOne.setApplicationName("TesteUm");
        entryOne.setUsername("UserUm");
        entryOne.setPassword("PassUm");
        entryTwo=new KeyEntry();
        entryTwo.setApplicationName("TesteDois");
        entryTwo.setUsername("UserDois");
        entryTwo.setPassword("PassDois");
        
    }
    
    @After
    public void tearDown() {
        
        File diretorio = new File("KeyChainTest.txt");
        diretorio.delete();
        
    }
    
    @Test
    public void PutTest(){
        
        kc.put(entryOne);
        kc.put(entryTwo);
        
        assertEquals(kc.find(entryOne.key()), entryOne);
        assertEquals(kc.find(entryTwo.key()), entryTwo);
        
    }
    @Test(expected = IllegalArgumentException.class)
    public void PutDuplicatedTest(){
        
        kc.put(entryOne);
        kc.put(entryOne);
    }
    
    @Test
    public void FindTest(){
        
        
        kc.put(entryOne);
        String entryOneKey = entryOne.key();
        
        assertEquals(entryOne, kc.find(entryOneKey));
    }
    
    @Test
    public void FindNullTest(){

        assertEquals(kc.find("Teste"), null);
    }
    
    @Test
    public void SaveTest() throws IOException{

        KeyChain kc1 = KeyChain.from(new File("KeyChainTest.txt"), new CipherTool("#wisper"));
        kc1.put(entryOne);
        kc1.save();
        
        assertEquals(KeyChain.from(new File("KeyChainTest.txt"), new CipherTool("#wisper")).find(entryOne.key()).toString(), entryOne.toString());
    }
    
    @Test
    public void AllEntriesTest(){
    
        List<KeyEntry> ke = new ArrayList<KeyEntry>();
        int inicio = 0, fim=0;
        
        for(KeyEntry k : kc.allEntries()){
        
            inicio++;
            
        }
        
        kc.put(entryOne);
        kc.put(entryTwo);
        
        for(KeyEntry k : kc.allEntries()){
        
            ke.add(k);
            fim++;
            
        }
        
        assertEquals(inicio, fim-2);
        assertTrue(ke.contains(entryOne));
        assertTrue(ke.contains(entryTwo));
        

    }
    
    @Test
    public void SortedEntriesTest(){
    
        KeyEntry ke = null;
        KeyEntryComparator kec = new KeyEntryComparator();
        boolean isSorted=true;
        
        for(KeyEntry ke1 : kc.sortedEntries()){
            
            if(ke != null && kec.compare(ke, ke1)>0){
            
                isSorted=false;
                
            }
            ke=ke1;
        }
        
        
        assertTrue(isSorted);
    }

    
}
