/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ico
 */
public class KeyEntryTest {
    private KeyEntry entryA, entryEmpty;
    
    public KeyEntryTest() {
    }
    
    @Before
    public void setUp() {
        entryA = new KeyEntry();
        entryA.setApplicationName("appx");
        entryA.setUsername("xx");
        entryA.setPassword("secret@@@");
        
    }
    
    @After
    public void tearDown() {
    }

   
    @Test( expected = IllegalArgumentException.class)
    public void testSetApplicationNameWithNull() {
        entryA.setApplicationName( null);
    }

    @Test
    public void testKey() {
        // the key is the application name
        assertEquals("failed to get existing key field", entryA.getApplicationName(), "appx");
    }

    @Test
    public void testFormatAsCsv() {
        String expects = "appx" + KeyEntry.FIELDS_DELIMITER + "xx" + KeyEntry.FIELDS_DELIMITER + "secret@@@";
        assertEquals("failed to format entry to delimited string", entryA.formatAsCsv(), expects);
                   
    }

    @Test
    public void testToString() {
        String testInput="appx\txx\tsecret@@@";
        assertEquals(testInput, entryA.toString());
    }

    @Test
    public void testParse() {
        String csvTest = "appx" + KeyEntry.FIELDS_DELIMITER + "xx" + KeyEntry.FIELDS_DELIMITER + "secret@@@";
        KeyEntry ke = entryA.parse(csvTest);
        assertEquals(ke.toString(), entryA.toString());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAppNameNull(){
        entryA.setApplicationName(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAppNameEmpty(){
        entryA.setApplicationName("");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAppUsernameNull(){
        entryA.setUsername(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testUsernameEmpty(){
        entryA.setUsername("");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testPasswordNull(){
        entryA.setPassword(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testPasswordEmpty(){
        entryA.setPassword("");
    }
    
    @Test
    public void testAppNameReturn(){
        
        entryA.setApplicationName("Teste");
        assertEquals(entryA.getApplicationName(), "Teste");       
    }
    
    @Test
    public void testUsernameReturn(){
        
        entryA.setUsername("Bruno");
        assertEquals(entryA.getUsername(), "Bruno");       
    }
    
    @Test
    public void testPasswordReturn(){
        
        entryA.setPassword("passTest");
        assertEquals(entryA.getPassword(), "passTest");       
    }
    
}
