/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bruno
 */
public class CipherToolTest {
    
    private File usersFile;
    private KeyChain kc;
    
    public CipherToolTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws IOException {
        
        usersFile=new File("Keychain.txt");
        kc=KeyChain.from(usersFile, new CipherTool("#wisper"));
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void KeyTest() throws IOException{
    
        KeyChain kc1 = KeyChain.from(usersFile, new CipherTool("#wisper"));
        assertEquals(kc.toString(), kc1.toString());
        
    }
    
    @Test(expected = IOException.class)
    public void KeyWrongTest() throws IOException{
    
        KeyChain kc2 = KeyChain.from(usersFile, new CipherTool("#test"));
        assertEquals(kc.toString(), kc2.toString());
        
    }
}
